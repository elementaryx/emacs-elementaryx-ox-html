(use-package elementaryx-ox-base)

(use-package oc-csl :after ox-html)

(use-package citeproc :after ox-html)

(use-package ox-html
  :defer t
  :custom
  (org-html-prefer-user-labels t)
  ;; TODO Refine the html rendering of source code in batch mode
  ;; Read: https://emacs.stackexchange.com/questions/31439/how-to-get-colored-syntax-highlighting-of-code-blocks-in-asynchronous-org-mode-e
  ;; In our workflow, we enable css
  (org-html-htmlize-output-type 'css)
  ;; and rely on https://mfelsoci.gitlabpages.inria.fr/inria-org-html-themes/readtheorginria/css/htmlize.css
  ;; In more complicated scenarios it might be required to generate an ad hoc css via (org-html-htmlize-generate-css)
  ;; Conversely, in an interactive scenario one might want to fall back to: (setq org-html-htmlize-output-type 'inlince-css)
  )

(use-package htmlize :after ox-html)

(provide 'elementaryx-ox-html)
